﻿using System;
using Malana.DAL.Entities;
using Malana.DAL.Repositories;

namespace Malana.DAL.Commands
{
    public class CreateTaskCommand : TaskCommand
    {
        private const int MaxTasksCount = 10000;

        public CreateTaskCommand(ITaskRepository repository)
        {
            _repository = repository;
        }

        public bool Execute(TaskEntity task)
        {
            if (_repository.Count() < MaxTasksCount)
            {
                task.Name = task.Name.Length > MaxTaskNameLength ? task.Name.Substring(0, MaxTaskNameLength) : task.Name;
                task.Id = Convert.ToInt32(_repository.Create(task));
                return true;
            }

            return false;
        }
    }
}
