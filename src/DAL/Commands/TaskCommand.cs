﻿using Malana.DAL.Repositories;

namespace Malana.DAL.Commands
{
    public abstract class TaskCommand
    {
        protected ITaskRepository _repository;
        protected const int MaxTaskNameLength = 130;
    }
}
