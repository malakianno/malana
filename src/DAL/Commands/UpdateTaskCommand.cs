﻿using Malana.DAL.Entities;
using Malana.DAL.Repositories;

namespace Malana.DAL.Commands
{
    public class UpdateTaskCommand : TaskCommand
    {
        public UpdateTaskCommand(ITaskRepository repository)
        {
            _repository = repository;
        }

        public void Execute(int id, TaskEntity task)
        {
            task.Name = task.Name.Length > MaxTaskNameLength ? task.Name.Substring(0, MaxTaskNameLength) : task.Name;
            _repository.Update(id, task);
        }
    }
}
