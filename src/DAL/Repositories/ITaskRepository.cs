﻿using System.Collections.Generic;
using Malana.DAL.Entities;

namespace Malana.DAL.Repositories
{
    public interface ITaskRepository
    {
        IEnumerable<TaskEntity> Get();

        TaskEntity Get(int id);

        bool Exists(int id);

        void Update(int id, TaskEntity newTaskEntity);

        long Create(TaskEntity newTaskEntity);

        void Delete(int id);

        long Count();
    }
}
