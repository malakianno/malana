﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Malana.DAL.Entities;
using Microsoft.Data.Sqlite;

namespace Malana.DAL.Repositories
{
    public class DefaultTaskRepository : ITaskRepository
    {
        public IEnumerable<TaskEntity> Get()
        {
            var result = new List<TaskEntity>();
            using (var connection = new SqliteConnection(new SqliteConnectionStringBuilder { DataSource = "database.db" }.ToString()))
            {
                connection.Open();
                var selectCommand = connection.CreateCommand();
                selectCommand.CommandText = @"
                                            SELECT
                                                * 
                                            FROM
                                                Tasks";
                using (var reader = selectCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var deadlineDate = new DateTime();
                        if (DateTime.TryParseExact(reader.GetString(2), "yyyy-MM-dd hh:mm:ss",
                            CultureInfo.InvariantCulture, DateTimeStyles.None, out deadlineDate))
                        {
                            result.Add(new TaskEntity()
                            {
                                Id = Convert.ToInt32(reader.GetString(0)),
                                Name = reader.GetString(1),
                                Deadline = deadlineDate,
                                IsCompleted = Convert.ToBoolean(reader.GetString(3))
                            });
                        }
                    }
                }
            }

            return result;
        }

        public TaskEntity Get(int id)
        {
            TaskEntity result = null;
            using (var connection = new SqliteConnection(new SqliteConnectionStringBuilder { DataSource = "database.db" }.ToString()))
            {
                connection.Open();
                var selectCommand = connection.CreateCommand();
                selectCommand.CommandText = string.Format(@"
                                                            SELECT
                                                                *
                                                            FROM
                                                                Tasks
                                                            WHERE
                                                                Id = {0}", id
                                                          );
                using (var reader = selectCommand.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var deadlineDate = new DateTime();
                        var tmp = reader.GetString(2);
                        if (DateTime.TryParseExact(reader.GetString(2), "yyyy-MM-dd hh:mm:ss",
                            CultureInfo.InvariantCulture, DateTimeStyles.None, out deadlineDate))
                        {
                            result = new TaskEntity()
                            {
                                Id = Convert.ToInt32(reader.GetString(0)),
                                Name = reader.GetString(1),
                                Deadline = deadlineDate,
                                IsCompleted = Convert.ToBoolean(reader.GetString(3))
                            };
                        }
                    }
                }
            }

            return result;
        }

        public bool Exists(int id)
        {
            TaskEntity result = null;
            using (var connection = new SqliteConnection(new SqliteConnectionStringBuilder { DataSource = "database.db" }.ToString()))
            {
                connection.Open();
                var selectCommand = connection.CreateCommand();
                selectCommand.CommandText = string.Format(@"
                                                            SELECT
                                                                COUNT(*)
                                                            FROM
                                                                Tasks
                                                            WHERE
                                                                Id = {0}", id
                                                          );
                return (long) selectCommand.ExecuteScalar() > 0;
            }
        }

        public void Update(int id, TaskEntity newTaskEntity)
        {
            using (
                var connection =
                    new SqliteConnection(new SqliteConnectionStringBuilder {DataSource = "database.db"}.ToString()))
            {
                connection.Open();
                var updateCommand = connection.CreateCommand();
                updateCommand.CommandText = string.Format(@"
                                                            UPDATE
                                                                Tasks
                                                            SET
                                                                Id = {0},
                                                                Name = '{1}',
                                                                Deadline = '{2}',
                                                                IsCompleted = '{3}'
                                                            WHERE
                                                                Id = {4}", 
                                                                newTaskEntity.Id,
                                                                newTaskEntity.Name,
                                                                newTaskEntity.Deadline.ToString("yyyy-MM-dd hh:mm:ss"),
                                                                newTaskEntity.IsCompleted,
                                                                id
                    );
                updateCommand.ExecuteNonQuery();
            }
        }

        public long Create(TaskEntity newTaskEntity)
        {
            using (
                var connection =
                    new SqliteConnection(new SqliteConnectionStringBuilder { DataSource = "database.db" }.ToString()))
            {
                connection.Open();
                var createCommand = connection.CreateCommand();
                createCommand.CommandText = string.Format(@"
                                                            INSERT INTO
                                                                Tasks(
                                                                    Name,
                                                                    Deadline,
                                                                    IsCompleted
                                                                )
                                                            VALUES(
                                                                '{0}',
                                                                '{1}',
                                                                '{2}'
                                                            );
                                                            SELECT last_insert_rowid()",
                                                                newTaskEntity.Name,
                                                                newTaskEntity.Deadline.ToString("yyyy-MM-dd hh:mm:ss"),
                                                                newTaskEntity.IsCompleted
                    );
                return (long)createCommand.ExecuteScalar();
            }
        }

        public void Delete(int id)
        {
            using (
                var connection =
                    new SqliteConnection(new SqliteConnectionStringBuilder { DataSource = "database.db" }.ToString()))
            {
                connection.Open();
                var deleteCommand = connection.CreateCommand();
                deleteCommand.CommandText = string.Format(@"
                                                            DELETE FROM
                                                                Tasks
                                                            WHERE
                                                                Id = {0}
                                                            ", id
                    );
                deleteCommand.ExecuteNonQuery();
            }
        }

        public long Count()
        {
            TaskEntity result = null;
            using (var connection = new SqliteConnection(new SqliteConnectionStringBuilder { DataSource = "database.db" }.ToString()))
            {
                connection.Open();
                var selectCommand = connection.CreateCommand();
                selectCommand.CommandText = string.Format(@"
                                                            SELECT
                                                                COUNT(*)
                                                            FROM
                                                                Tasks
                                                            ");
                return (long)selectCommand.ExecuteScalar();
            }
        }
    }
}
