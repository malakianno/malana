﻿namespace Malana.WebApp.ViewModels
{
    public class TaskViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Deadline { get; set; }
        public bool IsCompleted { get; set; }
    }
}
