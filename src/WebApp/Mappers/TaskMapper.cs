﻿using System;
using System.Globalization;
using Malana.DAL.Entities;
using Malana.WebApp.ViewModels;

namespace Malana.WebApp.Mappers
{
    public class TaskMapper
    {
        public TaskViewModel MapToViewModel(TaskEntity taskEntity)
        {
            return new TaskViewModel()
            {
                Id = taskEntity.Id,
                Name = taskEntity.Name,
                Deadline = taskEntity.Deadline.ToString("dd.MM.yyyy"),
                IsCompleted = taskEntity.IsCompleted
            };
        }

        public TaskEntity MapToEntity(TaskViewModel taskViewModel)
        {
            var deadlineDate = new DateTime();
            if (DateTime.TryParseExact(taskViewModel.Deadline, "dd.MM.yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out deadlineDate))
                return new TaskEntity()
                {
                    Id = taskViewModel.Id,
                    Name = taskViewModel.Name,
                    Deadline = deadlineDate,
                    IsCompleted = taskViewModel.IsCompleted
                };
            else
            {
                throw new Exception("Unknown format of deadline date");
            }
        }
    }
}
