import welcomePageControllerName from './controllers/welcomePageController'
import tasksPageControllerName from './controllers/tasksPageController'
import reducers from './reducers/reducers'

config.$inject = ['$stateProvider', '$urlRouterProvider', '$ngReduxProvider'];

export default function config($stateProvider, $urlRouterProvider, $ngReduxProvider) {
    $ngReduxProvider.createStoreWith(reducers);

	$stateProvider
		.state('/', {
			visibleName: 'Welcome',
			url: '/',
			templateUrl: '/views/welcome.html',
			controller: welcomePageControllerName
		})
		.state('/tasks', {
			visibleName: 'Tasks',
			url: '/tasks',
			templateUrl: '/views/tasks.html',
			controller: tasksPageControllerName
		})
	$urlRouterProvider.otherwise('/tasks');
}