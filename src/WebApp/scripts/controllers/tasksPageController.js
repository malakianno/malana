import controllersModuleName from './module'
import { addTask, changeIsCompletedStatus, removeTask, setTasksStorage, changeFilter } from '../actions/tasksActions'
import { saveNewTaskAreaVisibleStatus, saveTaskName, saveDeadlineDate, tasksAreLoading, tasksAreLoaded } from '../actions/tasksPageActions'
import FILTERS from '../components/task/tasks-filters'
import TimeHelper from '../helpers/timeHelper'
import tasksPageFactoryName from '../factories/tasksFactory'
import TASKS_STATUS from '../components/taskPage/task-page-status'
import Task from '../components/task/task'

const tasksPageControllerName = 'tasksPageController'

function tasksPageController($scope, $ngRedux, tasksPageFactoryName) {
	function configureScope() {
		$scope.dispatchFilter = function() {
			$ngRedux.dispatch(changeFilter($scope.filter));
		}

		$scope.dispatchTaskName = function() {
			$ngRedux.dispatch(saveTaskName($scope.newTaskName));
		}

		$scope.dispatchDeadlineDate = function() {
			$ngRedux.dispatch(saveDeadlineDate($scope.newDeadlineDate));
		}

		$scope.showAddNewTaskArea = function() {
			$ngRedux.dispatch(saveNewTaskAreaVisibleStatus(true));
		}

		$scope.saveTask = function() {
			var newTask = new Task(0, $scope.newTaskName, TimeHelper.toDate(angular.copy($scope.newDeadlineDate)), false);
			tasksPageFactoryName.create(newTask).success(function(response) {
				newTask.Id = response;
				$ngRedux.dispatch(addTask(newTask));
				$ngRedux.dispatch(saveNewTaskAreaVisibleStatus(false));
				resetTaskNameAndDeadline();
			});
		}

		$scope.removeTask = function(id) {
			tasksPageFactoryName.remove(id).success(function() {
				$ngRedux.dispatch(removeTask(id));
			});
		}

		$scope.completedStatusIsChanged = function(task) {
			tasksPageFactoryName.update(task).success(function() {
				$ngRedux.dispatch(changeIsCompletedStatus(task.Id));
			});
		}

		$scope.filters = FILTERS;
	}

	function loadTasks() {
		$ngRedux.dispatch(tasksAreLoading());
		tasksPageFactoryName.getAll().then(
			function(response) {
				$ngRedux.dispatch(setTasksStorage(response.data));
				$ngRedux.dispatch(tasksAreLoaded());
			}
		);
	}

	function filterTasks(tasks, filter) {
		const currentDate = TimeHelper.getCurrentDate();
		switch(filter) {
			case FILTERS.SHOW_ALL: 
				return angular.copy(tasks);
			case FILTERS.SHOW_TODAY:
				return angular.copy(tasks).filter(function(task) {
					return task.Deadline == currentDate;
				});
			case FILTERS.SHOW_TODAY_ACTIVE:
				return angular.copy(tasks).filter(function(task) {
					return task.Deadline == currentDate && !task.IsCompleted;
				});
			case FILTERS.SHOW_COMPLETED: 
				return angular.copy(tasks).filter(function(task) {
					return task.IsCompleted;
				});
			case FILTERS.SHOW_ACTIVE: 
				return angular.copy(tasks).filter(function(task) {
					return !task.IsCompleted;
				});
		}
	}

	function resetTaskNameAndDeadline() {
		$scope.newTaskName = '';
		$scope.dispatchTaskName();
		$scope.newDeadlineDate = new Date();
		$scope.dispatchDeadlineDate();
	}

	function mapStateToScope(state) {
        return {
            userName: state.common.username,
            tasks: filterTasks(state.todos.tasksStorage, state.todos.filter),
            filter: state.todos.filter,
            newTaskName: state.tasksPage.taskName,
            newDeadlineDate: state.tasksPage.deadlineDate,
            isAddNewTaskAreaVisible: state.tasksPage.isAddTaskAreaVisible,
            isTasksLoaded: state.tasksPage.tasksStatus === TASKS_STATUS.LOADED
        };
    }

    function initialize() {
    	configureScope();
		$ngRedux.connect(mapStateToScope)($scope);
		if(!$scope.isTasksLoaded) {
			loadTasks();
		}
    }

    initialize();
}

tasksPageController.$inject = ['$scope', '$ngRedux', tasksPageFactoryName];
angular.module(controllersModuleName).controller(tasksPageControllerName, tasksPageController);

export default tasksPageControllerName;