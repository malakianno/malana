import controllersModuleName from '../controllers/module'

const tasksPageFactoryName = 'tasksPageFactory';

angular.module(controllersModuleName).factory(tasksPageFactoryName, function($http) {
	var getAll = function() {
		return $http.get('/api/tasks');
	}

	var create = function(task) {
		return $http.post('/api/tasks', task);
	}

	var update = function(task) {
		return $http.put('/api/tasks', task);
	}

	var remove = function(id) {
		return $http.delete('/api/tasks/' + id);
	}

	return {
		getAll: getAll,
		create: create,
		update: update,
		remove: remove
	}
});

export default tasksPageFactoryName;