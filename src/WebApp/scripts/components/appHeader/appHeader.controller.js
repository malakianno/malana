appHeaderController.$inject = ['$scope', '$ngRedux'];

function appHeaderController($scope, $ngRedux) {
    function mapStateToScope(state) {
        return {
            username: state.common.username,
        };
    }

    function initialize() {
	    $scope.isAppHeaderVisible = function() {
	        return true;
    	};

    	$ngRedux.connect(mapStateToScope)($scope);
    }

    initialize();
}

export { appHeaderController };