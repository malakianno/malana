import { appHeaderController } from './appHeader.controller.js'

function headerDirectiveFactory() {
	var directive = {
		restrict: 'E',
		templateUrl: './templates/appHeader.html',
		scope: true,
		replace: true,
		controller: appHeaderController
	};

	return directive;
}

export default headerDirectiveFactory;