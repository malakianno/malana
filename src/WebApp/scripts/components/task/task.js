 class Task {
	 constructor(id, text, date, isCompleted) {
		this.Id = id;
		this.Name = text;
		this.Deadline = date;
		this.IsCompleted = isCompleted;
	}
}

export default Task