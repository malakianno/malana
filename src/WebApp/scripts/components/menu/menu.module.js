import menuDirectiveFactory from './menu.directive'

export default angular.module('app.menu', ['ui.router']).directive('menu', menuDirectiveFactory).name;