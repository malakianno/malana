import { ADD_TASK, CHANGE_IS_COMPLETED_STATUS_TASK, REMOVE_TASK, SET_TASKS_STORAGE, CHANGE_FILTER } from '../actions/tasksActions'
import FILTERS from '../components/task/tasks-filters'
import TasksHelper from '../helpers/tasksHelper'
import TimeHelper from '../helpers/timeHelper'
import Task from '../components/task/task'

const initialState = {
	day: TimeHelper.getCurrentDate(),
	filter: FILTERS.SHOW_ACTIVE,
	tasksStorage: []
}

function todos(state = initialState, action) {
	var newState = angular.copy(state);
	switch(action.type) {
		case ADD_TASK:
			newState.tasksStorage.push(action.payload.task);
			break;
		case CHANGE_IS_COMPLETED_STATUS_TASK:
			newState.tasksStorage = TasksHelper.changeIsCompletedStatusTask(newState.tasksStorage, action.payload.id);
			break;
		case REMOVE_TASK:
			newState.tasksStorage = TasksHelper.removeTask(newState.tasksStorage, action.payload.id);
			break;
		case CHANGE_FILTER:
			newState.filter = action.payload.filter;
			break;
		case SET_TASKS_STORAGE:
			newState.tasksStorage = action.payload.tasksStorage;
			break;
		default: return state;
	}

	return newState;
}

export default todos