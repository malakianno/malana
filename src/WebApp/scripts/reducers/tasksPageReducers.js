import { CHANGE_TASK_NAME, CHANGE_DEADLINE_DATE, CHANGE_TASK_AREA_VISIBLE_STATUS, TASKS_ARE_LOADING, TASKS_ARE_LOADED } from '../actions/tasksPageActions'
import TASKS_STATUS from '../components/taskPage/task-page-status'

const initialState = {
	tasksStatus: TASKS_STATUS.LOADING,
	taskName: '',
	deadlineDate: new Date(),
	isAddTaskAreaVisible: false
}

function tasksPage(state = initialState, action) {
	var newState = angular.copy(state);
	switch(action.type) {
		case CHANGE_TASK_NAME:
			newState.taskName = action.payload.taskName;
			break;
		case CHANGE_DEADLINE_DATE:
			newState.deadlineDate = action.payload.deadlineDate;
			break;
		case CHANGE_TASK_AREA_VISIBLE_STATUS:
			newState.isAddTaskAreaVisible = action.payload.isAddTaskAreaVisible;
			break;
		case TASKS_ARE_LOADING:
			newState.tasksStatus = TASKS_STATUS.LOADING;
			break;
		case TASKS_ARE_LOADED:
			newState.tasksStatus = TASKS_STATUS.LOADED;
			break;
		default: return state;
	}

	return newState;
}

export default tasksPage;