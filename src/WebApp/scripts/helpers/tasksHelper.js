class TasksHelper {
	static getNewId(tasks) {
		var result = 0;
		tasks.sort(function(first, second) {
						if(first.id < second.id) {
							return -1;
						} else if(first.id > second.id) {
							return 1;
						} else {
							return 0;
						}
					}
		);
		result = tasks.length > 1 ? tasks[tasks.length - 1].id + 1 : 0;

		return result;
	};

	static changeIsCompletedStatusTask(tasks, id) {
		tasks.forEach(function (task) {
			if(task.Id == id) {
				task.IsCompleted = !task.IsCompleted;
			}
		});

		return tasks;
	};

	static removeTask(tasks, id) {
		const result = tasks.filter(function (task) {
			return task.Id !== id;
		});

		return result;
	};
}

export default TasksHelper;