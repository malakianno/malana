export const ADD_TASK = 'ADD_TASK'
export const CHANGE_IS_COMPLETED_STATUS_TASK = 'CHANGE_IS_COMPLETED_STATUS_TASK'
export const REMOVE_TASK = 'REMOVE_TASK'
export const CHANGE_FILTER = 'CHANGE_FILTER'
export const SET_TASKS_STORAGE = 'SET_TASKS_STORAGE'

export function addTask(task) {
	return {
		type: ADD_TASK,
		payload: {
			task: task,
		}
	}
}

export function removeTask(id) {
	return {
		type: REMOVE_TASK,
		payload: {
			id: id
		}
	}
}

export function changeIsCompletedStatus(id) {
	return {
		type: CHANGE_IS_COMPLETED_STATUS_TASK,
		payload: {
			id: id
		}
	}
}

export function setTasksStorage(tasksStorage) {
	return {
		type: SET_TASKS_STORAGE,
		payload: {
			tasksStorage: tasksStorage
		}
	}
}

export function changeFilter(filter) {
	return {
		type: CHANGE_FILTER,
		payload: {
			filter: filter
		}
	}
}