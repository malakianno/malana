export const CHANGE_TASK_NAME = 'CHANGE_TASK_NAME'
export const CHANGE_DEADLINE_DATE = 'CHANGE_DEADLINE_DATE'
export const CHANGE_TASK_AREA_VISIBLE_STATUS = 'CHANGE_TASK_AREA_VISIBLE_STATUS'
export const TASKS_ARE_LOADING = 'TASKS_ARE_LOADING'
export const TASKS_ARE_LOADED = 'TASKS_ARE_LOADED'

export function saveTaskName(name) {
	return {
		type: CHANGE_TASK_NAME,
		payload: {
			taskName: name
		}
	}
}

export function saveDeadlineDate(date) {
	return {
		type: CHANGE_DEADLINE_DATE,
		payload: {
			deadlineDate: date
		}
	}
}

export function saveNewTaskAreaVisibleStatus(isVisible) {
	return {
		type: CHANGE_TASK_AREA_VISIBLE_STATUS,
		payload: {
			isAddTaskAreaVisible: isVisible
		}
	}
}

export function tasksAreLoading() {
	return {
		type: TASKS_ARE_LOADING
	}
}

export function tasksAreLoaded() {
	return {
		type: TASKS_ARE_LOADED
	}
}