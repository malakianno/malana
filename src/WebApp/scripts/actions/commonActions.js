export const SAVE_USERNAME = 'SAVE_USERNAME'

export function saveUserName(username) {
	return {
		type: SAVE_USERNAME,
		payload: { 
			username: username
		}
	}
}