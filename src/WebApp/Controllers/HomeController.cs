﻿using Microsoft.AspNet.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Malana.WebApp.Controllers
{
    [Route("/")]
    public class HomeController : Controller
    {
        // GET: api/values
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
    }
}
