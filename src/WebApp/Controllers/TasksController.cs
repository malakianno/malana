﻿using System;
using System.Linq;
using Malana.DAL.Commands;
using Microsoft.AspNet.Mvc;
using Malana.DAL.Repositories;
using Malana.WebApp.ViewModels;
using Malana.WebApp.Mappers;

namespace Malana.WebApp.Controllers
{
    [Route("api/[controller]")]
    public class TasksController : Controller
    {
        private readonly ITaskRepository _repository;
        private readonly TaskMapper _mapper;
        private readonly CreateTaskCommand _createCommand;
        private readonly UpdateTaskCommand _updateCommand;

        public TasksController()
        {
            _repository = new DefaultTaskRepository();
            _mapper = new TaskMapper();
            _createCommand = new CreateTaskCommand(_repository);
            _updateCommand = new UpdateTaskCommand(_repository);
        }

        // GET api/tasks
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                return Json(_repository.Get().Select(t => _mapper.MapToViewModel(t)));
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(500);
            }
        }

        // GET api/tasks/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            try
            {
                var task = _repository.Get(id);
                if (task == null)
                {
                    return new HttpStatusCodeResult(204);
                }
                return Json(_mapper.MapToViewModel(task));
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(500);
            }
        }

        // POST api/tasks
        [HttpPost]
        public ActionResult Post([FromBody]TaskViewModel taskViewModel)
        {
            try
            {
                if (taskViewModel == null)
                {
                    return new HttpStatusCodeResult(400);
                }
                var taskEntity = _mapper.MapToEntity(taskViewModel);
                if (_createCommand.Execute(taskEntity))
                {
                    return Json(taskEntity.Id);
                }
                else
                {
                    return new HttpStatusCodeResult(507);
                }
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(500);
            }
        }

        // PUT api/tasks/5
        [HttpPut]
        public ActionResult Put([FromBody]TaskViewModel taskViewModel)
        {
            try
            {
                if (taskViewModel == null)
                {
                    return new HttpStatusCodeResult(400);
                }
                var taskEntity = _mapper.MapToEntity(taskViewModel);
                _updateCommand.Execute(taskEntity.Id, taskEntity);
                return new HttpStatusCodeResult(204);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(500);
            }
        }

        // DELETE api/tasks/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
                return new HttpStatusCodeResult(204);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(500);
            }
        }
    }
}
